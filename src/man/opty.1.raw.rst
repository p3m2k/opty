#######
${SLUG}
#######

${DESCRIPTION}
==============

:author: ${AUTHOR_NAME}
:contact: ${AUTHOR_CONTACT}
:copyright: © ${COPYRIGHT_YEARS} ${COPYRIGHT_OWNER} <${COPYRIGHT_CONTACT}>
:manual_section: 1
:version: ${RELEASE_VERSION}
:date: ${RELEASE_DATE}



Synopsis
--------

${SLUG} [-h] [-v] [-V] [-n] [-b] [-f] [-q] [-c CFG] [-C PATTERN] COMMAND



Configuration
-------------

``opty`` reads an INI-style configuration file.

Each section name is treated as a Perl-compatible regular expression
(PCRE) pattern that is matched against the command format string passed
to opty.  If a section name matches the string, the value of the "all"
variable is used to evaluate the string.  If more than one section name
matches, the value of those sections' "all" variables will be
concatenated in the order they appear in the config file before being
used to evaluate the command format string.

A class name PCRE pattern can be provided via the `-C` option.  The
value of any variable (in matching sections) whose name is matched by
the class name pattern will also be used when evaluating the command
format string.



Description
-----------

``opty`` performs automatic command line option injection.  It’s
basically a sort of ``printf`` for command lines.  It takes a command in
the style of a printf format string, searches for matching options in a
config file, and executes the command after evaluating the format
string, using any options found in the config file as printf data
arguments.



Options
-------

-h
    Print usage.
-v
    Print version.
-V
    Verbose mode
-n
    Dry run mode
-b
    Do not use Bash printf builtin
-f
    Use only first matching command pattern
-q
    Do not quote printf data arguments
-c
    Path to config file
-C
    Config name pattern



Exit Status
-----------

0
    Success
1
    Failure

``opty`` will exit with a failure status when usage errors are
encountered, and when no config section is found to match the specified
command.



Files
-----

| ~/.opty.ini
| ${XDG_CONFIG_HOME}/opty.ini

    Configuration file.



Examples
--------

Given the following config file:

.. code:: ini

   [youtube-dl]
   all     = --format='worstvideo+worstaudio/worst'
   audio   = --extract-audio --audio-format=mp3

   [youtube-dl.*youtube.com.*playlist]
   all = --output "%(playlist_title)s/%(playlist_index)d-%(title)s-%(id)s.%(ext)s"

... opty enables you to call ``youtube-dl`` with just the URL of the
media you wish to download:

.. code:: console

   $ opty -n youtube-dl %s https://www.youtube.com/watch?v=Z3ZAGBL6UBA
   Dry run mode requested.
   youtube-dl --format='worstvideo+worstaudio/worst' https://www.youtube.com/watch?v=Z3ZAGBL6UBA

.. code:: console
   
   $ opty -n -C audio youtube-dl %s https://www.youtube.com/watch?v=Z3ZAGBL6UBA
   Dry run mode requested.
   youtube-dl --format='worstvideo+worstaudio/worst' --extract-audio --audio-format=mp3 https://www.youtube.com/watch?v=Z3ZAGBL6UBA

.. code:: console
   
   $ opty -n youtube-dl %s https://www.youtube.com/playlist?list=PLeSae__IT29fd3N2qP6OBYpJnbx67YlT9
   Dry run mode requested.
   youtube-dl --format='worstvideo+worstaudio/worst' --output "%(playlist_title)s/%(playlist_index)d-%(title)s-%(id)s.%(ext)s" https://www.youtube.com/playlist?list=PLeSae__IT29fd3N2qP6OBYpJnbx67YlT9

All of this works best with a shell alias:

.. code:: bash

   alias ytdl="opty -n youtube-dl %s "

Then, you can omit the options and ``printf``-formatted command, and
just specify a URL:

.. code:: console

   $ ytdl https://www.youtube.com/watch?v=Z3ZAGBL6UBA

And if you really want to get fancy, define a shell function instead of
an alias:

.. code:: bash

   ytdl() {
       cd ${HOME}/Downloads
       opty youtube-dl %s $@
   }



Bugs & Known Limitations
------------------------

Mind your command format string conversion specifiers!  Absent the
``-q`` option (unquoted ``printf`` data arguments), only one format
specifier (``%s``) is required.  Excess specifiers will evaluate to
an empty string.

When using ``opty`` with its ``-q`` option, it becomes even more
important to use the correct number of format specifiers.  Too many
specifiers will behave as described above; too few and ``printf`` will
evaluate the command format string multiple times, resulting in
unexpected output:

.. code:: console

   $ opty -nQ -C audio youtube-dl %s https://www.youtube.com/watch?v=Z3ZAGBL6UBA
   Dry run mode requested.
   printf will be called with unquoted arguments.
   youtube-dl --format='worstvideo+worstaudio/worst' https://www.youtube.com/watch?v=Z3ZAGBL6UBAyoutube-dl --extract-audio https://www.youtube.com/watch?v=Z3ZAGBL6UBAyoutube-dl --audio-format=mp3 https://www.youtube.com/watch?v=Z3ZAGBL6UBA

The *dry run* option (``-n``) is very helpful when learning how to use
``opty``!



See Also
--------

- confget(1)
- printf(3)
