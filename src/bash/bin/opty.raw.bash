#!/usr/bin/env bash

# ${DESCRIPTION}
#
# Copyright (c) ${COPYRIGHT_YEARS} ${COPYRIGHT_OWNER} (${COPYRIGHT_CONTACT})
# SPDX-License-Identifier: ${LICENSE_SPDX_ID}



# Important Stuff
#

set -o nounset  # Disallow unset variables and parameters.
set -o noglob   # Disable pathname expansion
set -o pipefail # Pipelines return value is exit value of last failing command
enable printf





# Constants
#

readonly myname="$(basename "${BASH_SOURCE[0]}")"  # $BASH_SOURCE for compatability w/ bats testing.
readonly usage_short="$myname [-h] [-v] [-V] [-n] [-b] [-f] [-q] [-c CFG] [-C PATTERN] COMMAND"
readonly usage_long="$usage_short
OPTIONS
    -h    Print usage
    -v    Print version
    -V    Verbose mode
    -n    Dry run mode
    -b    Do not use Bash printf builtin
    -f    Use only first matching command pattern
    -q    Do not quote printf data arguments
    -c    Path to config file
    -C    Class name pattern"

readonly version="${RELEASE_VERSION}"

readonly exit_success=0
readonly exit_error=1

readonly return_success=0
readonly return_error=1





# Defaults
#

# Make command line options global for sake of bats testing.
declare -gi verbose=0
declare -gi dry_run=0
declare -gi quote_printf_args=1
declare -g  config_file_locs='${HOME}/.${SLUG}.ini ${XDG_CONFIG_HOME:-${HOME}/.config}/${SLUG}.ini'
declare -g  config_file=""
declare -g  class_name_pattern=""
declare -gi first_match_only=0





# Subroutines
#


echo_verbose() {
    # Echo output to stderr if verbose option is enabled.

    if (( verbose )) ; then
        echo "$@" >&2
    fi
}



echo_func_err() {
    # Echo function error message to stderr.

    echo "${FUNCNAME[1]}: $@" >&2
}



maybe() {
    # Execute command unless dry run mode enabled; if dry run mode, echo
    # output to stdout.

    local -r cmd="$@"

    local -r trapped_signals="INT TERM QUIT"

    if (( dry_run )) ; then
        echo "$cmd"
    else
        trap mop_up $trapped_signals
        echo_verbose "Executing command: $cmd"
        eval $cmd &
        local -i pid=$!
        wait $pid  # Is $pid even necessary here?
        trap - $trapped_signals
        wait $pid
    fi
}



mop_up() {
    # Handle any trapped signals.  This usually amounts to killing child
    # processes and deleting any temp files.

    echo_verbose "Mopping up..."

    local -r pids="$(jobs -p)"
    if [[ -n $pids ]] ; then
        echo_verbose "Terminating child processes: '$pids'"
        kill $pids >&2
    fi

    exit $exit_error
}



find_config_file() {
    # Look for a readable config file at any/all of the paths specified
    # in $config_file_locs.

    local config_file
    local -i found=0

    for f_raw in $config_file_locs ; do
        f_cooked="$(eval echo "$f_raw")"
        if [[ -r "$f_cooked" ]] ; then
            config_file="$f_cooked"
            found=1
            break
        fi
    done

    if [[ $found -eq 1 ]] ; then
        echo "$config_file"
        return $return_success
    else
        return $return_error
    fi
}



get_command_patterns() {
    # Returns newline-delimited list of all config section names other
    # than "Opty".

    local section
    local class_name_pattern

    confget -f "$config_file" -q sections | grep -iv opty
}



get_command_pattern_options() {
    # Takes section name and optional class name pattern.
    # Returns newline-delimited list of config section variable values
    # ("all" plus any whose name matches the optional class name
    # pattern).

    local section
    local class_name_pattern

    if [[ $# -lt 1 ]]; then
        echo_func_err "Missing arguments"
        return $exit_error
    else
        section="$1"
        class_name_pattern="${2:-}"
    fi

    local confget_opts="-f \"$config_file\" -s \"$section\""
    eval confget $confget_opts -nLx "^all$"
    if [[ -n "$class_name_pattern" ]] ; then
        eval confget $confget_opts -nLx "$class_name_pattern"
    fi
}



get_command_options() {
    # Takes command format string and optional class name pattern.
    # Returns string of config section variable values ("all" plus any
    # whose name matches the optional class name pattern) for every
    # section whose name (PCRE pattern) matches $command.

    local command
    local class_name_pattern

    if [[ $# -lt 1 ]]; then
        echo_func_err "Missing arguments"
        return $exit_error
    else
        command="$1"
        class_name_pattern="${2:-}"
    fi

    local pattern
    local options
    local -i match=0
    for pattern in $(get_command_patterns); do
        if echo "$command" | grep -q --perl-regexp "$pattern" ; then
            match=1
            options+="$(get_command_pattern_options "$pattern" "$class_name_pattern") "
            (( first_match_only ))  &&  break
        fi
    done

    if (( match )) ; then
        echo ${options%% }
        return 0
    else
        return 1
    fi
}



parse_cli_opts() {
    # Process command line options.

    readonly cli_opts="hvVnbfqc:C:"
    local opt OPTIND OPTARG OPTERR

    while getopts ":${cli_opts}" opt; do
        case $opt in
            h)
                echo "$usage_long"  # Need quotes to preserve newlines!
                exit $exit_success
                ;;
            v)
                echo "$myname $version"
                exit $exit_success
                ;;
            V)
                verbose=1
                echo "Verbose mode requested." >&2
                ;;
            n)
                dry_run=1
                echo "Dry run mode requested." >&2
                ;;
            b)
                enable -n printf
                echo "Disabling Bash built-in version of printf." >&2
                ;;
            f)
                first_match_only=1
                echo "Using only first matching command pattern." >&2
                ;;
            q)
                quote_printf_args=0
                echo "printf will be called with unquoted arguments." >&2
                ;;
            c)
                if [[ ! -r "$OPTARG" ]] ; then
                    echo "Unreadable config file: $OPTARG" >&2
                    exit $exit_error
                else
                    config_file="$OPTARG"
                fi
                ;;
            C)
                class_name_pattern="$OPTARG"
                ;;
            :)
                echo "Dude, where's my arg? ($OPTARG)"
                echo "$usage_short"
                exit $exit_error
                ;;
            \?)
                echo "Invalid option: $OPTARG"
                echo "$usage_short"
                exit $exit_error
                ;;
        esac
    done

    return $((OPTIND-1))  # Number of options to discard.
}



main() {
    parse_cli_opts "$@"  ;  shift $?

    if [[ -z "$config_file" ]] ; then
        config_file=$(find_config_file)
        if [[ $? -eq 1 ]] ; then
            echo "No config file found!'" >&2
            exit $exit_error
        fi
    fi
    echo_verbose "Using config file: $config_file"

    if [[ ! $@ ]]; then
        echo "Tell me what to do!"
        echo "$usage_short"
        exit $exit_error
    else
        declare -g cmd_raw="$@"
    fi


    local cmd_opts
    cmd_opts="$(get_command_options "$cmd_raw" "$class_name_pattern")"

    if [[ $? -eq 0 ]] ; then
        if (( quote_printf_args )) ; then
            maybe "$(printf "$cmd_raw" "$cmd_opts")"
        else
            maybe "$(printf "$cmd_raw" $cmd_opts)"
        fi
    else
        echo "Command has no matching config entries.'" >&2
        exit $exit_error
    fi

    exit $exit_success
}





if [[ "${BASH_SOURCE[0]}" == "${0}" ]] ; then
    main "$@"
fi
