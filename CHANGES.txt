v.1.1.0 (2022-09-02)
===============================================================================
50f109f6  Unit tests should mock dependencies
5a899f96  Fix ytdl() shell func example in manpage
91f65576  Validate user-specified config file
99256c30  Set default value for $XDG_CONFIG_HOME
a5e28db8  main() should exit when no config file found
cb5ad418  Expand verbose mode to echo fully-evaluated command line prior to execution
df9458a6  Delete leftover boilerplate in main()

v.1.0.0 (2022-09-01)
===============================================================================
Initial release.
