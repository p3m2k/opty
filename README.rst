#############################################
Opty: Automatic command line option injection
#############################################


Introduction
============

*Opty* performs automatic command line option injection.  It’s basically
a sort of ``printf`` for command lines.  It takes a command in the style
of a printf format string, searches for matching options in a config
file, evaluates the format string using any options found, and then
executes the resulting command.

For example, given the following config file:

.. code:: ini

   [youtube-dl]
   all     = --format='worstvideo+worstaudio/worst'
   audio   = --extract-audio --audio-format=mp3

   [youtube-dl.*youtube.com.*playlist]
   all = --output "%(playlist_title)s/%(playlist_index)d-%(title)s-%(id)s.%(ext)s"

... Opty enables the following:

.. code:: console

   $ opty -n youtube-dl %s https://www.youtube.com/watch?v=Z3ZAGBL6UBA
   Dry run mode requested.
   youtube-dl --format='worstvideo+worstaudio/worst' https://www.youtube.com/watch?v=Z3ZAGBL6UBA

.. code:: console
   
   $ opty -n -C audio youtube-dl %s https://www.youtube.com/watch?v=Z3ZAGBL6UBA
   Dry run mode requested.
   youtube-dl --format='worstvideo+worstaudio/worst' --extract-audio --audio-format=mp3 https://www.youtube.com/watch?v=Z3ZAGBL6UBA

.. code:: console
   
   $ opty -n youtube-dl %s https://www.youtube.com/playlist?list=PLeSae__IT29fd3N2qP6OBYpJnbx67YlT9
   Dry run mode requested.
   youtube-dl --format='worstvideo+worstaudio/worst' --output "%(playlist_title)s/%(playlist_index)d-%(title)s-%(id)s.%(ext)s" https://www.youtube.com/playlist?list=PLeSae__IT29fd3N2qP6OBYpJnbx67YlT9


Installation
============

*Opty* is a Bash_ shell script.  Almost every Linux system includes
Bash.  Users of other Unix-like_ systems may need to install it before
using this program.

In addition to Bash, Opty requires confget_, ``grep``, and ``printf``.

.. _confget: https://devel.ringlet.net/textproc/confget/

There are two easy ways to install Opty:

- Packages_ are available for users of Debian and its derivatives
  (Ubuntu, etc.).  Users of Red Hat, Fedora and friends: stay tuned!

- Download an archive of the latest release and run ``make install``:

  .. code:: console

     # wget -O - https://www.nellump.net/downloads/opty_latest.tgz | tar xz
     # cd opty/
     # make install

bats_ is required to run the automated test suite (of interest only to
programmers).

.. _Bash: https://www.gnu.org/software/bash/
.. _Unix-like: http://www.linfo.org/unix-like.html
.. _packages : https://www.nellump.net/computers/free_software
.. _bats: https://github.com/bats-core/bats-core



Usage
=====

See the `manpage <src/man/opty.1.rst>`_.



Future Plans
============

None at this time.



Copying
=======

Everything here is copyright © 2022 Paul Mullen
(with obvious exceptions for any `fair use`_ of third-party content).

Everything here is offered under the terms of the MIT_ license.

    Basically, you can do whatever you want as long as you include the
    original copyright and license notice in any copy of the
    software/source.

.. _fair use: http://fairuse.stanford.edu/overview/fair-use/what-is-fair-use/
.. _MIT: https://tldrlegal.com/license/mit-license



Contact
=======

Feedback is (almost) always appreciated.  Send e-mail to
pm@nellump.net.
