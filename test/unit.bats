load lib/assertions.bash
load lib/utils.bash
load lib/mocker.bash
load lib/common.bash
load meta.bash



# Test Support
#

setup_file() {
    # NB: Variables in setup_file() and teardown_file() must be exported!
    # Global vars declared in sourced SWUT can be overridden in test cases with
    # `local VARNAME=VAL` declarations.

    assert_dependencies $dependencies
}


fake_cmd() {
    local -r first="$1"
    local -r second="$2"

    echo "First: \"$first\""
    echo "Second: \"$second\""

    if [[ "$first" == "one" ]]  &&  [[ "$second" == "one" ]] ; then
        echo "Snake eyes!  Failure so sad."
        return 1
    else
        echo "Success!"
        return 0
    fi
} ; export -f fake_cmd



# Common Tests
#

@test "echo_verbose()" {
    local output="This is only a test."

    source_swut "$swut"
    local -r verbose=1
    run echo_verbose "$output"

    assert_status $expected_status_success
    assert_lines_qty 1
    [[ "${lines[0]}" == "$output" ]]  # FIXME: confirm stderr
}


@test "echo_func_err()" {
    local err_msg="This is only a test."

    source_swut "$swut"
    run echo_func_err "$err_msg"

    assert_status $expected_status_success
    assert_lines_qty 1
    # NB: The calling function name depends on bats internals!
    [[ "${lines[0]}" == "bats_merge_stdout_and_stderr: $err_msg" ]]  # FIXME: confirm stderr
}


@test "maybe(), success" {
    source_swut "$swut"
    run maybe fake_cmd "one" "two"

    assert_status $expected_status_success
    assert_lines_qty 3
    [[ "${lines[0]}" == "First: \"one\"" ]]
    [[ "${lines[1]}" == "Second: \"two\"" ]]
    [[ "${lines[2]}" == "Success!" ]]
}


@test "maybe(), failure" {
    source_swut "$swut"
    run maybe fake_cmd "one" "one"

    assert_status $expected_status_error
    assert_lines_qty 3
    [[ "${lines[0]}" == "First: \"one\"" ]]
    [[ "${lines[1]}" == "Second: \"one\"" ]]
    [[ "${lines[2]}" == "Snake eyes!  Failure so sad." ]]
}


@test "maybe(), dry run mode" {
    local dry_run=1  # Necessary to override global dry_run in SWUT.

    source_swut "$swut"
    run maybe fake_cmd "seven" "three"

    assert_status $expected_status_success
    assert_lines_qty 1
    [[ "${lines[0]}" == "fake_cmd seven three" ]]
}


@test "find_config_file(), success" {
    local assets_d="test/assets"
    local fake_config_f="${assets_d}/bar"
    touch "${fake_config_f}"

    source_swut "$swut"
    local config_file_locs="${assets_d}/foo ${assets_d}/bar"
    run find_config_file

    rm -f "${fake_config_f}"

    assert_status $expected_status_success
    assert_lines_qty 1
    [[ "${lines[0]}" == "${fake_config_f}" ]]
}


@test "find_config_file(), failure" {
    local assets_d="test/assets"

    source_swut "$swut"
    local config_file_locs="${assets_d}/foo ${assets_d}/bar"
    run find_config_file

    assert_status $expected_status_error
    assert_lines_qty 0
}



# Script-specific tests
#

@test "get_command_patterns()" {
    source_swut "$swut"
    config_file="$test_config_file"
    run get_command_patterns

    assert_status $expected_status_success
    assert_lines_qty 2
    [[ "${lines[0]}" == "eenie" ]]
    [[ "${lines[1]}" == "meenie" ]]
}


@test "get_command_pattern_options(), missing args" {
    source_swut "$swut"
    config_file="$test_config_file"
    run get_command_pattern_options

    assert_status $expected_status_error
    assert_lines_qty 1
    [[ "${lines[0]}" == "get_command_pattern_options: Missing arguments" ]]
}


@test "get_command_pattern_options(), unmatched command" {
    source_swut "$swut"
    config_file="$test_config_file"
    run get_command_pattern_options "eenies"

    assert_status $expected_status_success
    assert_lines_qty 0
}


@test "get_command_pattern_options()" {
    source_swut "$swut"
    config_file="$test_config_file"
    run get_command_pattern_options "eenie"

    assert_status $expected_status_success
    assert_lines_qty 1
    [[ "${lines[0]}" == "fee fi" ]]
}


@test "get_command_pattern_options(), 0 class match" {
    source_swut "$swut"
    config_file="$test_config_file"
    run get_command_pattern_options "meenie" "^shazam$"

    assert_status $expected_status_success
    assert_lines_qty 1
    [[ "${lines[0]}" == "fo fum" ]]
}


@test "get_command_pattern_options(), 1 class match" {
    source_swut "$swut"
    config_file="$test_config_file"
    run get_command_pattern_options "eenie" "^foo$"

    assert_status $expected_status_success
    assert_lines_qty 2
    [[ "${lines[0]}" == "fee fi" ]]
    [[ "${lines[1]}" == "'higgeldy'" ]]
}


@test "get_command_pattern_options(), 2 class match" {
    source_swut "$swut"
    config_file="$test_config_file"
    run get_command_pattern_options "eenie" "foo"

    assert_status $expected_status_success
    assert_lines_qty 3
    [[ "${lines[0]}" == "fee fi" ]]
    [[ "${lines[1]}" == "'higgeldy'" ]]
    [[ "${lines[2]}" == "\"The cheese stands alone.\"" ]]
}


@test "get_command_options(), missing args" {
    source_swut "$swut"
    config_file="$test_config_file"
    run get_command_options

    assert_status $expected_status_error
    assert_lines_qty 1
    [[ "${lines[0]}" == "get_command_options: Missing arguments" ]]
}


@test "get_command_options(), 0 pattern match" {
    source_swut "$swut"
    config_file="$test_config_file"
    run get_command_options "weinie"

    assert_status $expected_status_error
    assert_lines_qty 0
}


@test "get_command_options(), 1 pattern match" {
    source_swut "$swut"
    config_file="$test_config_file"
    run get_command_options "eenie"

    assert_status $expected_status_success
    assert_lines_qty 1
    [[ "${lines[0]}" == "fee fi" ]]
}


@test "get_command_options(), 2 pattern match" {
    source_swut "$swut"
    config_file="$test_config_file"
    run get_command_options "meenie"

    assert_status $expected_status_success
    assert_lines_qty 1
    [[ "${lines[0]}" == "fee fi fo fum" ]]
}


@test "get_command_options(), 1 pattern match, 0 class match" {
    source_swut "$swut"
    config_file="$test_config_file"
    run get_command_options "eenie" "shazam"

    assert_status $expected_status_success
    assert_lines_qty 1
    [[ "${lines[0]}" == "fee fi" ]]
}


@test "get_command_options(), 1 pattern match, 1 class match" {
    source_swut "$swut"
    config_file="$test_config_file"
    run get_command_options "eenie" "^foo$"

    assert_status $expected_status_success
    assert_lines_qty 1
    [[ "${lines[0]}" == "fee fi 'higgeldy'" ]]
}


@test "get_command_options(), 1 pattern match, 2 class match" {
    source_swut "$swut"
    config_file="$test_config_file"
    run get_command_options "eenie" "foo"

    assert_status $expected_status_success
    assert_lines_qty 1
    [[ "${lines[0]}" == "fee fi 'higgeldy' \"The cheese stands alone.\"" ]]
}


@test "get_command_options(), 2 pattern match, 0 class match" {
    source_swut "$swut"
    config_file="$test_config_file"
    run get_command_options "meenie" "shazam"

    assert_status $expected_status_success
    assert_lines_qty 1
    [[ "${lines[0]}" == "fee fi fo fum" ]]
}


@test "get_command_options(), 2 pattern match, 1 class match" {
    source_swut "$swut"
    config_file="$test_config_file"
    run get_command_options "meenie" "^foo$"

    assert_status $expected_status_success
    assert_lines_qty 1
    [[ "${lines[0]}" == "fee fi 'higgeldy' fo fum 'piggeldy'" ]]
}


@test "get_command_options(), 2 pattern match, 2 class match" {
    source_swut "$swut"
    config_file="$test_config_file"
    run get_command_options "meenie" "foo"

    assert_status $expected_status_success
    assert_lines_qty 1
    [[ "${lines[0]}" == "fee fi 'higgeldy' \"The cheese stands alone.\" fo fum 'piggeldy'" ]]
}





# vim: syntax=bash tw=0
