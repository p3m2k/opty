assert_dependencies() {
    [[ $# -lt 1 ]]  &&  return

    local -r dependencies="$@"

    for cmd in "$dependencies" ; do
        which $cmd >/dev/null
        if [[ $? -ne 0 ]] ; then
            # FIXME: Does BATS die before we get here (because `set -e`)?
            echo "# Missing dependency: $cmd" >&3
        fi
    done
}



assert_status() {
    local -ir retval=$1

    [[ "$status" -eq $retval ]]
}



assert_lines_qty() {
    local -ir qty=$1

    [[ "${#lines[*]}" -eq $qty ]]
}



assert_file_found() {
    local -r file="$1"

    stat "$1" >/dev/null 2>&1
}



assert_file_not_found() {
    # FIXME: Is it just me or is this kind of pointless?
    # Why not just [[ ! -a $file ]] ?

    local -r file="$1"

    # bats runs with bash's "errexit" option set.  Disable it just long
    # enough to run the `stat` command that should fail if the function
    # is to return true/0.
    set +o errexit
    stat "$file" >/dev/null 2>&1
    local -ir rv=$?
    set -o errexit

    if [[ $rv -eq 1 ]] ; then
        return 0
    else
        return 1
    fi
}



assert_file_cmp_string() {
    local -r file_path="$1"
    local -r expected_str="$2"

    cmp -- "$file_path" <(echo "$expected_str")
}



assert_file_cmp_file() {
    local -r file_path_l="$1"
    local -r file_path_r="$2"

    cmp -- "$file_path_l" "$file_path_r"
}



assert_file_grep_string() {
    local -r file_path="$1"
    local -r expected_str="$2"

    grep -q -- "$expected_str" "$file_path"
}
