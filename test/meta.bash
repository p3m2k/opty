swut="opty"
dependencies="confget grep printf"

expected_version="1.1.0"

expected_status_success=0
expected_status_error=1

expected_usage_short="$swut [-h] [-v] [-V] [-n] [-b] [-f] [-q] [-c CFG] [-C PATTERN] COMMAND"
expected_usage_long="$expected_usage_short
OPTIONS
    -h    Print usage
    -v    Print version
    -V    Verbose mode
    -n    Dry run mode
    -b    Do not use Bash printf builtin
    -f    Use only first matching command pattern
    -q    Do not quote printf data arguments
    -c    Path to config file
    -C    Class name pattern"

expected_output_dry_run_opt="Dry run mode requested."
expected_output_first_match_opt="Using only first matching command pattern."
expected_output_unquoted_printf_opt="printf will be called with unquoted arguments."
expected_output_no_bash_builtin_opt="Disabling Bash built-in version of printf."

test_config_file="test/assets/opty.ini"
