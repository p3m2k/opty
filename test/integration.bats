load lib/assertions.bash
load lib/utils.bash
load lib/mocker.bash
load lib/common.bash
load meta.bash



setup_file() {
    # NB: Variables in setup_file() and teardown_file() must be exported!

    assert_dependencies $dependencies

    export stdopts="-n -c $test_config_file"
}




# Common options
#

@test "no opts, no args" {
    run exec_swut $stdopts

    assert_status $expected_status_error
    assert_lines_qty 3
    [[ "${lines[0]}" == "$expected_output_dry_run_opt" ]]
    [[ "${lines[1]}" == "Tell me what to do!" ]]
    [[ "${lines[2]}" == "$expected_usage_short" ]]
}


@test "invalid opt" {
    run exec_swut -z

    assert_status $expected_status_error
    assert_lines_qty 2
    [[ "${lines[0]}" == "Invalid option: z" ]]
    [[ "${lines[1]}" == "$expected_usage_short" ]]
}


@test "missing opt arg" {
    run exec_swut -c

    assert_status $expected_status_error
    assert_lines_qty 2
    [[ "${lines[0]}" == "Dude, where's my arg? (c)" ]]
    [[ "${lines[1]}" == "$expected_usage_short" ]]
}


@test "help" {
    run exec_swut -h

    assert_status $expected_status_success
    assert_lines_qty 11  # bats ignores blank lines.
    [[ "$output" == "$expected_usage_long" ]]
}


@test "version" {
    run exec_swut -v

    assert_status $expected_status_success
    assert_lines_qty 1
    [[ "${lines[0]}" == "$swut $expected_version" ]]
}


@test "verbose option" {
    run exec_swut $stdopts -V

    assert_status $expected_status_error
    assert_lines_qty 5
    [[ "${lines[0]}" == "$expected_output_dry_run_opt" ]]
    [[ "${lines[1]}" == "Verbose mode requested." ]]
    [[ "${lines[2]}" == "Using config file: test/assets/opty.ini" ]]
    [[ "${lines[3]}" == "Tell me what to do!" ]]
    [[ "${lines[4]}" == "$expected_usage_short" ]]
}


@test "dry run option" {
    run exec_swut $stdopts

    assert_status $expected_status_error
    assert_lines_qty 3
    [[ "${lines[0]}" == "$expected_output_dry_run_opt" ]]
    [[ "${lines[1]}" == "Tell me what to do!" ]]
    [[ "${lines[2]}" == "$expected_usage_short" ]]
}


@test "unreadable config file" {
    local config_f="/dev/nada"
    run exec_swut -n -c "$config_f" nop

    assert_status $expected_status_error
    assert_lines_qty 2
    [[ "${lines[0]}" == "$expected_output_dry_run_opt" ]]
    [[ "${lines[1]}" == "Unreadable config file: $config_f" ]]
}



# Basic usage
#

@test "0 command match" {
    run exec_swut $stdopts high-ho %s

    assert_status $expected_status_error
    assert_lines_qty 2
    [[ "${lines[0]}" == "$expected_output_dry_run_opt" ]]
    [[ "${lines[1]}" == "Command has no matching config entries.'" ]]
}


@test "1 command match" {
    run exec_swut $stdopts eenie %s

    assert_status $expected_status_success
    assert_lines_qty 2
    [[ "${lines[0]}" == "$expected_output_dry_run_opt" ]]
    [[ "${lines[1]}" == "eenie fee fi" ]]
}


@test "1 command match, excess format specs" {
    run exec_swut $stdopts eenie %s-%s

    assert_status $expected_status_success
    assert_lines_qty 2
    [[ "${lines[0]}" == "$expected_output_dry_run_opt" ]]
    [[ "${lines[1]}" == "eenie fee fi-" ]]
}


@test "1 command match, quoted format spec" {
    run exec_swut $stdopts eenie \'%s\'

    assert_status $expected_status_success
    assert_lines_qty 2
    [[ "${lines[0]}" == "$expected_output_dry_run_opt" ]]
    [[ "${lines[1]}" == "eenie 'fee fi'" ]]
}


@test "1 command match, 1 class match" {
    run exec_swut $stdopts -C '^foo$' eenie %s

    assert_status $expected_status_success
    assert_lines_qty 2
    [[ "${lines[0]}" == "$expected_output_dry_run_opt" ]]
    [[ "${lines[1]}" == "eenie fee fi 'higgeldy'" ]]
}


@test "1 command match, 2 class match" {
    run exec_swut $stdopts -C foo eenie %s

    assert_status $expected_status_success
    assert_lines_qty 2
    [[ "${lines[0]}" == "$expected_output_dry_run_opt" ]]
    [[ "${lines[1]}" == "eenie fee fi 'higgeldy' \"The cheese stands alone.\"" ]]
}


@test "2 command match" {
    run exec_swut $stdopts meenie %s

    assert_status $expected_status_success
    assert_lines_qty 2
    [[ "${lines[0]}" == "$expected_output_dry_run_opt" ]]
    [[ "${lines[1]}" == "meenie fee fi fo fum" ]]
}


@test "2 command match, 1 class match" {
    run exec_swut $stdopts -C '^foo$' meenie %s

    assert_status $expected_status_success
    assert_lines_qty 2
    [[ "${lines[0]}" == "$expected_output_dry_run_opt" ]]
    [[ "${lines[1]}" == "meenie fee fi 'higgeldy' fo fum 'piggeldy'" ]]
}


@test "2 command match, 2 class match" {
    run exec_swut $stdopts -C foo meenie %s

    assert_status $expected_status_success
    assert_lines_qty 2
    [[ "${lines[0]}" == "$expected_output_dry_run_opt" ]]
    [[ "${lines[1]}" == "meenie fee fi 'higgeldy' \"The cheese stands alone.\" fo fum 'piggeldy'" ]]
}


@test "2 command match, first match only opt" {
    run exec_swut $stdopts -f meenie %s

    assert_status $expected_status_success
    assert_lines_qty 3
    [[ "${lines[0]}" == "$expected_output_dry_run_opt" ]]
    [[ "${lines[1]}" == "$expected_output_first_match_opt" ]]
    [[ "${lines[2]}" == "meenie fee fi" ]]
}


@test "2 command match, first match only opt, 2 class match" {
    run exec_swut $stdopts -f -C foo eenie %s

    assert_status $expected_status_success
    assert_lines_qty 3
    [[ "${lines[0]}" == "$expected_output_dry_run_opt" ]]
    [[ "${lines[1]}" == "$expected_output_first_match_opt" ]]
    [[ "${lines[2]}" == "eenie fee fi 'higgeldy' \"The cheese stands alone.\"" ]]
}



# prinf Options (-b, -q)
#

@test "1 command match, unquoted printf args opt" {
    run exec_swut $stdopts -q eenie %s-%s

    assert_status $expected_status_success
    assert_lines_qty 3
    [[ "${lines[0]}" == "$expected_output_dry_run_opt" ]]
    [[ "${lines[1]}" == "$expected_output_unquoted_printf_opt" ]]
    [[ "${lines[2]}" == "eenie fee-fi" ]]
}


@test "1 command match, unquoted printf args opt, quoted printf format" {
    run exec_swut $stdopts -q eenie \'%s-%s\'

    assert_status $expected_status_success
    assert_lines_qty 3
    [[ "${lines[0]}" == "$expected_output_dry_run_opt" ]]
    [[ "${lines[1]}" == "$expected_output_unquoted_printf_opt" ]]
    [[ "${lines[2]}" == "eenie 'fee-fi'" ]]
}


@test "1 command match, unquoted printf args opt, excess format specs" {
    run exec_swut $stdopts -q eenie %s-%s-%s

    assert_status $expected_status_success
    assert_lines_qty 3
    [[ "${lines[0]}" == "$expected_output_dry_run_opt" ]]
    [[ "${lines[1]}" == "$expected_output_unquoted_printf_opt" ]]
    [[ "${lines[2]}" == "eenie fee-fi-" ]]
}


@test "1 command match, unquoted printf args opt, insufficient format specs" {
    # Not something to test for; just confirming expected behavior.
    run exec_swut $stdopts -q eenie %s

    assert_status $expected_status_success
    assert_lines_qty 3
    [[ "${lines[0]}" == "$expected_output_dry_run_opt" ]]
    [[ "${lines[1]}" == "$expected_output_unquoted_printf_opt" ]]
    [[ "${lines[2]}" == "eenie feeeenie fi" ]]
}


@test "1 command match, external printf opt, unquoted printf args opt" {
    run exec_swut $stdopts -b -q eenie %s-%s

    assert_status $expected_status_success
    assert_lines_qty 4
    [[ "${lines[0]}" == "$expected_output_dry_run_opt" ]]
    [[ "${lines[1]}" == "$expected_output_no_bash_builtin_opt" ]]
    [[ "${lines[2]}" == "$expected_output_unquoted_printf_opt" ]]
    [[ "${lines[3]}" == "eenie fee-fi" ]]
}


@test "1 command match, external printf opt, unquoted printf args opt, quoted printf format" {
    run exec_swut $stdopts -b -q eenie \'%s-%s\'

    assert_status $expected_status_success
    assert_lines_qty 4
    [[ "${lines[0]}" == "$expected_output_dry_run_opt" ]]
    [[ "${lines[1]}" == "$expected_output_no_bash_builtin_opt" ]]
    [[ "${lines[2]}" == "$expected_output_unquoted_printf_opt" ]]
    [[ "${lines[3]}" == "eenie 'fee-fi'" ]]
}


@test "1 command match, external printf opt, unquoted printf args opt, excess format specs" {
    run exec_swut $stdopts -b -q eenie %s-%s-%s

    assert_status $expected_status_success
    assert_lines_qty 4
    [[ "${lines[0]}" == "$expected_output_dry_run_opt" ]]
    [[ "${lines[1]}" == "$expected_output_no_bash_builtin_opt" ]]
    [[ "${lines[2]}" == "$expected_output_unquoted_printf_opt" ]]
    [[ "${lines[3]}" == "eenie fee-fi-" ]]
}


@test "1 command match, external printf opt, unquoted printf args opt, insufficient format specs" {
    # Not something to test for; just confirming expected behavior.
    run exec_swut $stdopts -b -q eenie %s

    assert_status $expected_status_success
    assert_lines_qty 4
    [[ "${lines[0]}" == "$expected_output_dry_run_opt" ]]
    [[ "${lines[1]}" == "$expected_output_no_bash_builtin_opt" ]]
    [[ "${lines[2]}" == "$expected_output_unquoted_printf_opt" ]]
    [[ "${lines[3]}" == "eenie feeeenie fi" ]]
}






# vim: syntax=bash tw=0
