# Format:
# <source>	<destination>	<mode>	<owner>	<group>
# <source> is relative to Makefile $(SRC_DIR).
# <destination> is relative to Makefile $(DESTDIR).
# <mode>, <owner>, and <group> are optional.

bash/bin/${SLUG}		bin/${SLUG}		755
man/${SLUG}.1.gz	share/man/man1/${SLUG}.1.gz	644
